---
layout: default
title: Documentation
---

This project represent a Todo React/ Node application. It enables its users to do CRUD operations with a todo list.

## The server
The server-side is written in Node. 
To get the server up and running you need to follow these steps:
1. Make sure that npm is installed on your computer, if not install it.
2. Then cd into to_do_node and run npm install.
3. After all the dependencies are done installing cd into todosAPI and run nodemon app.js.
4. By default the server will running on port 8082, it can be changed inside app.js.